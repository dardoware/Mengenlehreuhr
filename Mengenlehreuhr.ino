#include <Wire.h>
#include "RTClib.h"
#include "FastLED.h"


const byte LEDS_DATA_PIN_0  = 2;
const byte LEDS_DATA_PIN_1  = 3;
const byte LEDS_DATA_PIN_2  = 4;
const byte LEDS_DATA_PIN_3  = 5;
const byte LEDS_DATA_PIN_4  = 6;

const byte LEDS_CLOCK_PIN_0 = 7;
const byte LEDS_CLOCK_PIN_1 = 8;
const byte LEDS_CLOCK_PIN_2 = 9;
const byte LEDS_CLOCK_PIN_3 = 10;
const byte LEDS_CLOCK_PIN_4 = 11;

const byte NUM_LEDS_CASILLA_FILA_0 = 5;
const byte NUM_LEDS_CASILLA_FILA_1 = 2;
const byte NUM_LEDS_CASILLA_FILA_2 = 2;
const byte NUM_LEDS_CASILLA_FILA_3 = 1;
const byte NUM_LEDS_CASILLA_FILA_4 = 2;

CRGB C_HORAS     = CRGB::Red;
CRGB C_MINUTOS_0 = CRGB::Blue;
CRGB C_MINUTOS_1 = CRGB::Green;
CRGB C_SEGUNDOS  = CRGB::Yellow;

//  **** FIN DE LA ZONA DE CONFIGURACION - Pasada esta linea no hay instrucciones (puede que antes de esta linea tampoco, soy vago, lo siento) :( ****

const byte NUM_CASILLAS_FILA_0 = 1;
const byte NUM_CASILLAS_FILA_1 = 4;
const byte NUM_CASILLAS_FILA_2 = 4;
const byte NUM_CASILLAS_FILA_3 = 11;
const byte NUM_CASILLAS_FILA_4 = 4;



const byte sizeFila_0 = NUM_LEDS_CASILLA_FILA_0 * NUM_CASILLAS_FILA_0;
const byte sizeFila_1 = NUM_LEDS_CASILLA_FILA_1 * NUM_CASILLAS_FILA_1;
const byte sizeFila_2 = NUM_LEDS_CASILLA_FILA_2 * NUM_CASILLAS_FILA_2;
const byte sizeFila_3 = NUM_LEDS_CASILLA_FILA_3 * NUM_CASILLAS_FILA_3;
const byte sizeFila_4 = NUM_LEDS_CASILLA_FILA_4 * NUM_CASILLAS_FILA_4;


CRGB fila_0[sizeFila_0];
CRGB fila_1[sizeFila_1];
CRGB fila_2[sizeFila_2];
CRGB fila_3[sizeFila_3];
CRGB fila_4[sizeFila_4];

const byte NUM_LEDS=sizeFila_0 + sizeFila_1 + sizeFila_2 + sizeFila_3 + sizeFila_4;
 
CRGB mengenlehreuhr[NUM_LEDS];


const long INTERVALO_DEBUG = 1000;
long tempDebug = 0; 

RTC_DS1307 reloj;

byte brillo = 32;

byte v_fila_1;
byte v_fila_2;
byte v_fila_3;
byte v_fila_4;

void setup() {
  Wire.begin();
  reloj.begin();
  Serial.begin(115200);
  FastLED.addLeds<WS2801, LEDS_DATA_PIN_0, LEDS_CLOCK_PIN_0, RGB>(fila_0, sizeFila_0).setCorrection(Typical8mmPixel);
  FastLED.addLeds<WS2801, LEDS_DATA_PIN_1, LEDS_CLOCK_PIN_1, RGB>(fila_1, sizeFila_1).setCorrection(Typical8mmPixel);
  FastLED.addLeds<WS2801, LEDS_DATA_PIN_2, LEDS_CLOCK_PIN_2, RGB>(fila_2, sizeFila_2).setCorrection(Typical8mmPixel);
  FastLED.addLeds<WS2801, LEDS_DATA_PIN_3, LEDS_CLOCK_PIN_3, RGB>(fila_3, sizeFila_3).setCorrection(Typical8mmPixel);  
  FastLED.addLeds<WS2801, LEDS_DATA_PIN_4, LEDS_CLOCK_PIN_4, RGB>(fila_4, sizeFila_4).setCorrection(Typical8mmPixel);

  Serial.println("Comenzando");
}



void loop () {
    DateTime ahora = reloj.now();
    unsigned long temporizador = millis();
    
    byte mm = ahora.minute();
    byte hh = ahora.hour();
    
    
    v_fila_1 = hh/5;
    v_fila_2 = hh%5;
    v_fila_3 = mm/5;
    v_fila_4 = mm%5;
    
    pintarRelojLeds();
    
    if(temporizador - tempDebug > INTERVALO_DEBUG) {
      tempDebug = temporizador; 
      SerialPrintTime(ahora); 
    }
    
    
    FastLED.show(brillo);
    
}

void SerialPrintTime(DateTime t) {
    Serial.print(t.hour(), DEC);
    Serial.print(':');
    Serial.print(t.minute(), DEC);
    Serial.print(':');
    Serial.print(t.second(), DEC);
    Serial.println();
    
    Serial.println(v_fila_1);
    Serial.println(v_fila_2);
    Serial.println(v_fila_3);
    Serial.println(v_fila_4);
 
    for (byte i= 0 ; i < sizeFila_0; i++) {
      Serial.print("Fila_0[");
      Serial.print(i); Serial.print("]: ");
      Serial.print(fila_0[i].r);
      Serial.print(",");
      Serial.print(fila_0[i].g);
      Serial.print(",");
      Serial.println(fila_0[i].b);     
    }   
    
    for (byte i= 0 ; i < sizeFila_1; i++) {
      Serial.print("Fila_1[");
      Serial.print(i);
      Serial.print("]: ");
      Serial.print(fila_1[i].r);
      Serial.print(",");
      Serial.print(fila_1[i].g);
      Serial.print(",");
      Serial.println(fila_1[i].b);     
    }
    
    for (byte i= 0 ; i < sizeFila_2; i++) {
      Serial.print("Fila_2[");
      Serial.print(i);
      Serial.print("]: ");
      Serial.print(fila_2[i].r);
      Serial.print(",");
      Serial.print(fila_2[i].g);
      Serial.print(",");
      Serial.println(fila_2[i].b); 
    }
    
    for (byte i= 0 ; i < sizeFila_3; i++) {
      Serial.print("Fila_3[");
      Serial.print(i);
      Serial.print("]: ");
      Serial.print(fila_3[i].r);
      Serial.print(",");
      Serial.print(fila_3[i].g);
      Serial.print(",");
      Serial.println(fila_3[i].b);      
    }
    
     for (byte i= 0 ; i < sizeFila_4; i++) {
      Serial.print("Fila_4["); Serial.print(i); Serial.print("]: "); Serial.print(fila_4[i].r); Serial.print(",");Serial.print(fila_4[i].g); Serial.print(",");Serial.println(fila_4[i].b);      
    }
    Serial.println("================================================================");
}


void pintarRelojLeds() {
  for (byte i=0; i < sizeFila_1; i++) {
    if (i < v_fila_1 * NUM_LEDS_CASILLA_FILA_1) { fila_1[i] = C_HORAS; } else {fila_1[i] = CRGB::Black; }
  }
  
    for (byte i=0; i < sizeFila_2; i++) {
    if (i < v_fila_2 * NUM_LEDS_CASILLA_FILA_2) { fila_2[i] = C_HORAS; } else {fila_2[i] = CRGB::Black; }
  }
  
  for (byte i=0; i < sizeFila_3; i++) {
    if (i < v_fila_3 * NUM_LEDS_CASILLA_FILA_3) { fila_3[i] = C_MINUTOS_0; } else {fila_3[i] = CRGB::Black; }
  }
  
    for (byte i=0; i < sizeFila_4; i++) {
    if (i < v_fila_4 * NUM_LEDS_CASILLA_FILA_4) { fila_4[i] = C_HORAS; } else {fila_4[i] = CRGB::Black; }
  }
}
